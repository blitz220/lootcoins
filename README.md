# lootcoins
## Selenium Project to claim monthly reward for Dragonrealms subscribers
This project executes completely within the Gitlab CI/CD ecosystem, in conjunction with the `selenium/standalone-chrome` docker image, to create an automated headless run.  You can execute the python script locally if you wish, just set up your own selenium environment.

## Fork the repository
You'll need to create your own repository by forking this one. That will allow you to set up the job defined below for your accounts.  For the job to execute, you will have to provide a credit card.  This job will barely scratch the surface of free hours provided, it is just a requirement that Gitlab has nowadays from people abusing free tier.

## Configure this project to run automatically
Thanks to VTCifer for figuring out what broke the loop; execution is back to normal.

#### Schedule a cron to execute monthly
To schedule this job to run on a recurring schedule, navigate to Build -> Pipeline schedules and create a New Schedule. I use a custom cron that runs on the 15th of the month at 3am ETC, but this would work with the standard monthly option also.

To provide the credentials for the job to run, configure the variables section to mount a `file` (not variable) named `list` which contains a new-line seperated set of `username:password` key/value pairs.  Once saved, the job will run on the set schedule.  See the image for example.

![CronExample](images/cron2.png)

